public class metadatacreator {
    public  void m(){
        list<DynamicCreationFieldMetadata__c> sd = new list<DynamicCreationFieldMetadata__c>();
        for(DynamicCreationFieldMetadata__c d:[select id from DynamicCreationFieldMetadata__c]){
            sd.add(d);
        }
        delete sd;
    }
    public void demom(){
        map<String,Schema.SObjectType> mobj = Schema.getGlobalDescribe();//getting the map of objects
        Map<String,Schema.SObjectField> mfields;
         mfields=mobj.get('human__c').getdescribe().fields.getMap();
        for(Schema.SObjectField fld :mFields.values()){
schema.describeFieldResult dfield = fld.getDescribe();
string fldType = string.valueOf(dfield.getType());   
    if (fldType == 'REFERENCE'){ // Lookup field!
     system.debug(string.valueOf(dfield.getReferenceTo()) + ' = ' + fldType);
    }}

    }
    public void insertm(){
        list<DynamicCreationFieldMetadata__c> l =new List<DynamicCreationFieldMetadata__c>();
        for(integer i=0;i<99;i++){
            DynamicCreationFieldMetadata__c m = new DynamicCreationFieldMetadata__c(SobjectName__c='human__c',DelField__c=true,sLength__c=20,SobjLookUp__c='account',Sfield__c='Testing'+i+'__c',slabel__c='TestingLabel'+i,sdatatype__c='Text');
     l.add(m);
        }
      insert l;  
    }
  static long startTime = System.currentTimeMillis(); //fetch starting time
    public static list<custom_error_logs__c> message= new list<custom_error_logs__c> ();  
   public static  boolean mess=false;
    
    public static String createSObj(String sobj, String sfield,String slabel, String sdatatype ,String lookObj, integer slength,String sessionId,Boolean DelField){
        
        map<String,Schema.SObjectType> mobj = Schema.getGlobalDescribe();//getting the map of objects
        Map<String,Schema.SObjectField> mfields;
        
        if(String.isNotBlank(sobj)&& mobj.containsKey(sobj)){
         mfields=mobj.get(sobj).getdescribe().fields.getMap();//getting the map of fields
            system.debug('mfields'+mfields);
        }
          else
         {
            return'Error:No sObject present';
        } 
        
        
        
        if(delField)//to delete the field we are the checking the boolean value if present then delete
        {
            system.debug('1');
            deleteField( sobj,sfield,sessionId);
            return '';
        }
      
       else if(delField==false && mfields.containsKey(sfield))//
       {
            return 'Error:Duplicate Field Exist';
        }
        //jitterbit dataloader
         else if(sdatatype=='LookUp' && String.isNotBlank(lookObj))
         {
             if(!mobj.containsKey(lookObj)){
                 return'Error:No look up Object present';
             }  
        // List<Schema.sObjectType> lfielddataType  = mfields.get(sfield).getDescribe().getReferenceTo();
           //  for(Schema.sObjectType s:lfielddataType){
               //  if(String.valueOf(s)==lookObj){
                // return 'Error:LookUP field  already exist';
            // }}
             
             createLookUpField( sobj,  sfield, slabel,  sdatatype , lookObj, sessionId);
              return '';
          }

        else if( sdatatype!='Lookup')
        {
            if(slength==null) {
                return 'Error:length of the text should be provided';
            }
            createField( sobj,  sfield, slabel,  sdatatype , slength, sessionId);
            return '';}
       
        else
        {                
           return 'Error:LookUp reference object cannot be empty';
        }
    }
  
@future(callout=true)
public static  void createField(String sobj, String sfield,String slabel, String sdatatype ,integer slength,String sessionId)
    {

          String   message1;
  MetadataService.MetadataPort service = createService(sessionId);
    
 List<MetadataService.Metadata> fields = new List<MetadataService.Metadata>();
   MetadataService.CustomField customField = new MetadataService.CustomField();
customField.fullName = sobj+'.'+sfield;
customField.label = slabel;
customField.type_x = sdatatype;
customField.length = slength;
        fields.add(customField);
MetadataService.SaveResult[] results =
    service.createMetadata(fields);       if(!results.isEmpty()){
        for (MetadataService.SaveResult result : results) {
            if (!result.Success) {
                 message1 =  'Error in creating field : Errors '+
                    'occured processing component ' ;   
                custom_error_logs__c errlog = new custom_error_logs__c(Name='metadataerror', error_description__c=message1);
                insert errlog;
                } 
            }
        }
    }
//    

@future(callout=true)
public static  void createLookUpField(String sobj, String sfield,String slabel, String sdatatype ,String lookObj,String sessionId)
    {
         map<String,Schema.SObjectType> mobj = Schema.getGlobalDescribe();
          String   message1;
    MetadataService.MetadataPort service = createService(sessionId);
 List<MetadataService.Metadata> fields = new List<MetadataService.Metadata>();
   MetadataService.CustomField customField = new MetadataService.CustomField();
   customField.fullName = sobj+'.'+sfield;
     customField.label = slabel;
     customField.type_x = sdatatype;
    customField.relationshipLabel = 'relationshipLabel';
      customField.relationshipName = 'relationshipName';
     customField.referenceTo = lookObj;
        fields.add(customField);
MetadataService.SaveResult[] results =
    service.createMetadata(fields);       if(!results.isEmpty()){
        for (MetadataService.SaveResult result : results) {
            if (!result.Success) {
                 message1 =  'Error in creating  LookUpfield : Errors '+
                    'occured processing component ' ;   
                custom_error_logs__c errlog = new custom_error_logs__c(Name='metadataerror', error_description__c=message1);
                insert errlog;
                }     
            }
        }
    
    }
//
     public static MetadataService.MetadataPort createService(String sessionId)
    {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = sessionId;
        return service;
    }
    @future(callout=true)
    public  static void createObj (set<id> ids,String sessionID)
    {string message1;
        MetadataService.MetadataPort service = createService(sessionId);
       MetadataService.CustomObject customObject = new MetadataService.CustomObject();
      List< MetadataService.Metadata> customObjectList = new list<MetadataService.Metadata>();
       List< MetadataService.CustomObject> customObjectL = new list<MetadataService.CustomObject>();
     List<MetadataService.SaveResult> results;
    for(DynamicCreationFieldMetadata__c dc:[select id,SobjectName__c,slabel__c from DynamicCreationFieldMetadata__c where id=:ids])
        {
        customObject.fullName = dc.SobjectName__c;
        customObject.label = dc.slabel__c;
        customObject.pluralLabel = dc.slabel__c+'s';
        customObject.nameField = new MetadataService.CustomField();
        customObject.nameField.type_x = 'Text';
        customObject.nameField.label = dc.slabel__c;
        customObject.deploymentStatus = 'Deployed';
        customObject.sharingModel = 'ReadWrite';
            customObjectList.add(customObject);
        
           system.debug('customobj'+customObjectList);
       results =service.createMetadata(customObjectList );
        }
        for (MetadataService.SaveResult result : results) {
            if (!result.Success) {
                 message1 =  'Error in Creating Object : Errors '+
                    'occured processing component ' ;   
                custom_error_logs__c errlog = new custom_error_logs__c(Name='metadataerror', error_description__c=message1);
                insert errlog;        
                }
            
            } 
        //if condition
    }
    
    @future(callout=true)
    public static void deleteField(String sobj,String sfield,String sessionId)
    {
        string message1;
        MetadataService.MetadataPort service = createService(sessionId);
        List<MetadataService.DeleteResult> results =
            service.deleteMetadata(
                'CustomField', new String[] { sobj+'.'+sfield });
        for (MetadataService.DeleteResult result : results) {
            if (!result.Success) {
                 message1 =  'Error in Delete: Errors '+
                    'occured processing component ' ;   
                custom_error_logs__c errlog = new custom_error_logs__c(Name='metadataerror', error_description__c=message1);
                insert errlog;        
                } 
            } 
        
    }
    
}