public class ScheduleBatchEnableYoutubeSearch implements schedulable {

public void execute(SchedulableContext sc ){

   BatchEnableYoutubeSearch oc = new BatchEnableYoutubeSearch();
 
  if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5 && !Test.IsRunningTest()){
   Database.executeBatch(oc,200);
   }
    else {
           Schedule_batch_ownerChange SCSchedule = new Schedule_batch_ownerChange();
            Datetime dt = Datetime.now() + (0.02084); // adding 30 mins
            String timeForScheduler = dt.format('s m H d M \'?\' yyyy');
            Id schedId = System.Schedule('Survey_SalesPro_Schedule_Opportunity_RETRY'+timeForScheduler,timeForScheduler,SCSchedule);
        }
//CronTrigger ct =[SELECT TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :sc.getTriggerId()];
//system.debug(''+ct);


}
}