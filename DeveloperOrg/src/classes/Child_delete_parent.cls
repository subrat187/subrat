public class Child_delete_parent {
 
    
    public static void Child_delete(List<Store__c> c){
        Set<ID> i = new Set<ID>();
        for( Store__c s:c) {
           i.add(s.Shopping_Mall__c);
        }
        
        List<Shopping_Mall__c> m =[Select Name,ID from Shopping_Mall__c where id in :i for update];
        Delete m;
    }
}