@isTest
public class Zip_populateTest {
    public static  testmethod void populate(){
   Add__c ad = new Add__c(name ='Accenture',billingcity__c ='lalitpur');
       
     Test.startTest();
        insert ad;
         Account a = new Account(name ='Accenture');
        insert a;
        
        account ab = [select name ,billingcity from account ];
        Test.stopTest();
        
    System.assertEquals(ab.billingcity,'lalitpur');
        
    }
}