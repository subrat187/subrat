global class Comparators implements comparable{
   global  Names name{get;set;}
    public Comparators (Names name){
       this.name= name; 
    }
    global integer compareTo( Object  o ){
        Comparators n = (Comparators) o;
        if(name.name.length()> n.name.name.length()){
                return -1;
            }
                else if (name.name.length() < n.name.name.length())
                    return 1;
                else{
                    return 0;
                }
            }    
}