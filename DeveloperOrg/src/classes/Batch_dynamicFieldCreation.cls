public class Batch_dynamicFieldCreation implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
public string sessionid;
   String query;
    private Set<Id> Ids;
    public Batch_dynamicFieldCreation(Set<Id> ids,string sessionid){
        this.ids=ids;
        this.sessionid=sessionid;
     query= 'select id,SobjectName__c,slabel__c,Sfield__c,sdatatype__c,SobjLookUp__c,sLength__c,DelField__c from DynamicCreationFieldMetadata__c where id IN :ids';
     system.debug('query'+query);
    }

public Database.querylocator start(Database.BatchableContext BC){
  return Database.getQueryLocator(query);
   }
 
public void execute(Database.BatchableContext BC, List<sObject> scope){
        String message1;
    List<custom_error_logs__c> lc = new list<custom_error_logs__c>();
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = sessionid;
     List<MetadataService.Metadata> fields = new List<MetadataService.Metadata>();
     MetadataService.CustomObject customObject = new MetadataService.CustomObject();
      MetadataService.CustomField customField = new MetadataService.CustomField();
   
     List<MetadataService.SaveResult> results;
    for( sobject sc:scope)
        {
         DynamicCreationFieldMetadata__c dc =(DynamicCreationFieldMetadata__c)sc;
           //if delete is true
            if(dc.DelField__c==true){
                system.debug('deleted in field');
                List<MetadataService.DeleteResult> delresults =
            service.deleteMetadata(
                'CustomField', new String[] { dc.SobjectName__c+'.'+dc.Sfield__c });
        
            }
            
            else if(dc.sdatatype__c!='Lookup'){
                 system.debug('TextField');
        customField.fullName = dc.SobjectName__c+'.'+dc.Sfield__c;
customField.label = Dc.slabel__c;
customField.type_x = dc.sdatatype__c;
customField.length = Integer.valueof(dc.sLength__c);
        fields.add(customField);
                   results = service.createMetadata(fields);
                fields.clear();
            if(!results.isEmpty()){
        for (MetadataService.SaveResult result : results) {
            if (!result.Success) {
                for(MetadataService.Error error : result.errors){
                    message1 =  'Error in creating field : Errors '+
                    'occured processing component '+error.message;   }
     custom_error_logs__c errlog = new custom_error_logs__c(Name='metadataerror', error_description__c=message1);
                lc.add(errlog);
              //  insert errlog;
            }
                }insert lc; 
                lc.clear();
            }
            }else{          
                 system.debug('lookup');
      customField.fullName = dc.SobjectName__c+'.'+dc.Sfield__c;
customField.label = Dc.slabel__c;
customField.type_x = dc.sdatatype__c;
    customField.relationshipLabel = 'relationshipLabel';
      customField.relationshipName = 'relationshipName';
     customField.referenceTo = dc.SobjLookUp__c;
        fields.add(customField);
            results = service.createMetadata(fields);
                fields.clear();
            if(!results.isEmpty()){
        for (MetadataService.SaveResult result : results) {
            if (!result.Success) {
                for(MetadataService.Error error : result.errors){
                    message1 =  'Error in creating field : Errors '+
                    'occured processing component '+error.message;   }
                 
                custom_error_logs__c errlog = new custom_error_logs__c(Name='metadataerror', error_description__c=message1);
               // insert errlog;
                lc.add(errlog);
            }
                }
                insert lc;
                lc.clear();
            }
        }
            
            } 
        //if condition
    
    
    }

     public void finish(Database.batchableContext bc){}


    

    
    
    
}