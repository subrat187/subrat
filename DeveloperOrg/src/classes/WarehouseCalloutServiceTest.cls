@isTest
private class WarehouseCalloutServiceTest {
    @testSetup
    public static void createdummyObjects(){
        Vehicle__c vname = new Vehicle__c(name='subratMR');
        product2 prod = new product2(name='Dell');
        insert vname;
        insert prod;
        case newcase = new case(Type='Repair',Origin='Phone',status='New',Vehicle__c=vname.id,Equipment__c=prod.id);
        insert newcase;
    }
    
    public static  testmethod void getProd(){
         Test.SetMock(HttpCallOutMock.class, new WarehouseCalloutServiceMock());
        case caserec =[select id,status from case limit 1];
        caserec.status='closed';
        update caserec;
        WarehouseCalloutService.runWarehouseEquipmentSync();
         WarehouseSyncSchedule SCSchedule = new WarehouseSyncSchedule(); 
            String timeForScheduler = '20 30 8 10 2 ?';
            Id schedId = System.Schedule('Survey_SalesPro_Schedule_Opportunity_RETRY',timeForScheduler,SCSchedule);
    }
  
}