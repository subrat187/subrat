@isTest
public class Opp_Test {

    public static  testMethod void   opp_Test1(){
        pageReference pr =Page.v2;
        Test.setCurrentPage(pr);   
    List<Opportunity> oppList = new List<Opportunity>();
    Opportunity opp =TestFactory.getOpportunity();
     Opportunity opp1 = new Opportunity();
        opp1.name='subratopp';
        opp1.StageName='Closed Won';
        opp1.Type='Existing Customer - Upgrade';
        opp1.Amount=500.00;
        opp1.CloseDate=system.today();
        oppList.add(opp1);
        insert opp;
       oppList.add(opp);
        insert opp1;
      ApexPages.StandardController ass = new ApexPages.StandardController(opp);
        opp o1 = new opp(ass);
        o1.setopw(oppList);
        List<opportunity> o2=o1.getopl();
        for(opportunity o3:o2)
        System.assertEquals('subratopp1', o3.Name);
                           
    }
    
}