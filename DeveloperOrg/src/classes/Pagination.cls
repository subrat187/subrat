public class Pagination {

   private integer counter=0; 
   private integer list_size=20; 
   public integer total_size; 

   public Pagination() {
   total_size = [select count() from Account]; 
   }

   public Account[] getNumbers() {
      try {
         Account[] numbers = [select Name, phone 
                                from Account
                                order by Name 
                                limit :list_size 
                                offset :counter];

         return numbers;
      } catch (QueryException e) {
         ApexPages.addMessages(e);   
         return null;
      }
   }

   public PageReference Beginning() { 
      counter = 0;
      return null;
   }

   public PageReference Previous() { 
      counter -= list_size;
      return null;
   }

   public PageReference Next() { 
      counter += list_size;
      return null;
   }

   public PageReference End() {
      counter = total_size - math.mod(total_size, list_size);
      return null;
   }

   public Boolean getDisablePrevious() { 

      if (counter>0)return false; else return true;
   }

   public Boolean getDisableNext() { 
      if (counter + list_size < total_size) return false; else return true;
   }

   public Integer getTotal_size() {
      return total_size;
   }

   public Integer getPageNumber() {
      return counter/list_size + 1;
   }

   public Integer getTotalPages() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
   }
}