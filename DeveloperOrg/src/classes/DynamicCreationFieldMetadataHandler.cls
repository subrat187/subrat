public class DynamicCreationFieldMetadataHandler {
    
    public static String createdynamicData(String sobj, String sfield,String slabel, String sdatatype ,String lookObj, integer slength,String sessionId,Boolean DelField){
        
        map<String,Schema.SObjectType> mobj = Schema.getGlobalDescribe();//getting the map of objects
        Map<String,Schema.SObjectField> mfields;
        
        if(String.isNotBlank(sobj)&& mobj.containsKey(sobj)){
            mfields=mobj.get(sobj).getdescribe().fields.getMap();//getting the map of fields
            system.debug('mfields'+mfields);
        }
        else
        {
            return'Error:No sObject present';
        } 
        if( delField==false && mfields.containsKey(sfield))
        {
            return 'Error:Duplicate Field Exist';
        }
        //jitterbit dataloader
        if( slength==null && sdatatype!='Lookup')
        {
            
            return 'Error:length of the text should be provided';
        }
        
        if( lookObj!=null && sdatatype=='Lookup') {
            for(Schema.SObjectField fld :mFields.values()){
                schema.describeFieldResult dfield = fld.getDescribe();
                
                for(Schema.SObjectType s :dfield.getReferenceTo()){
                    if(String.valueOf(s)==lookObj){
                         return 'Error:Look up to the provided object already present ';}
                }
                   
            }}
        
    
    if( lookObj==null && sdatatype=='Lookup'){
        
        return 'Error:LookUp reference object cannot be empty';
    }
    return '';
}
public static void createSObj(Set<id>ids, String sessionid){
    system.debug('createobject');
    database.executeBatch(new Batch_dynamicObjectCreation(ids,sessionid),9);
    
}

public static void createField(Set<id>ids, String sessionid){
    system.debug('createfield');
    database.executeBatch(new Batch_dynamicFieldCreation(ids,sessionid),9);
    
}



}