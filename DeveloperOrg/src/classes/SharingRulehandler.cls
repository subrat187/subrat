public class SharingRulehandler {

    public boolean isExecuting;
    public boolean isbefore;
    public boolean isafter;
    public boolean isupdate;
    public boolean isinsert;
    public integer size;
    public boolean isundelete;
    public List<vehicles__c> triggernew;
    public List<vehicles__C> triggerold;
    Static boolean makepublic =false;
   
    public SharingRulehandler(boolean isExecuting, boolean isbefore, boolean isafter, boolean isinsert,boolean isupdate, boolean isundelete,List<Vehicles__c> triggernew, List<Vehicles__c> triggerold,integer size){
    this.isExecuting=isExecuting;
   this.isbefore=isbefore;
    this.isafter=isafter;
   this.isupdate=isupdate;
     this.isinsert=isinsert;
   this.size=size;
     this.isundelete=isundelete;
   this.triggernew=triggernew;
   this.triggerold=triggerold;
    }
    
    public void onafterInsert(){
        if(isafter && isinsert){
             Map<ID,ID> m = new Map<ID,ID>();
         ID i = [SELECT Id FROM Group where Name='poudyalgroup'].id;
          
            for(vehicles__c vech :triggernew){
                if(vech.Make_public__c ==true){
                    makepublic= true;
                }
                
                m.put(vech.id,i);       
            }
           
            if(makepublic=true){
                sharingRUleUtility.SobjectsMethod('vehicles__share', m);
            }
             
         
        }
    }
        public void onafterupdate(){
           
        }
    public void onbeforeInsert(List<Vehicles__C> vech){
        
    }
     public void onbeforeUpdate(List<Vehicles__C> vech){
         
     }
     public void onafterundelete(List<Vehicles__C> vech){
         
     }
     
    
    
    
}