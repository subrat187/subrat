public class LeadProcessor implements Database.Batchable<sObject>, Database.Stateful {
    public Database.QueryLocator start(Database.BatchableContext bc) {
         return Database.getQueryLocator('select id,LeadSource from Lead');
    }
    
    public void execute(Database.BatchableContext bc, List<Lead> scope){
        list<lead> led = new list<lead>();
        for(Lead leadrec :scope){
            leadrec.LeadSource='Dreamforce';
            led.add(leadrec);
        }   
        
        if(!led.isEmpty()){
           update led; 
        }   
    }
    
    public void finish(Database.BatchableContext bc){
    }
}