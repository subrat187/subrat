// to update the count of contacts in account
public class AccountProcessor {
    public static void callFuture(){
     List<Account> accToUpdt =[select id from account];
       list<id> accntId = new list<Id>();
        for(Account acnt:accToUpdt){
            accntId.add(acnt.id);
        }
        countContacts(accntId);
    }
    
    @future 
    public static void countContacts(list<ID> AccntIds){
     map<id,Integer> countdet = new map<id,Integer>();
    for(aggregateResult cont :[select accountid,count(id) cnt from contact  where accountid In :AccntIds  Group by accountid ]){
        countdet.put((ID)cont.get('accountId'), (Integer)cont.get('cnt'));     
        }
        // to update the account records
       List<Account> accToUpdt =[select Number_of_Contacts__c from account where id in :countdet.keySet()];
       list<account> accToUpdate = new list<account>();
        For(Account acc :accToUpdt ){
            if(countdet.containsKey(acc.id)){
             acc.Number_of_Contacts__c=countdet.get(acc.id);  
                accToUpdate.add(acc);
            } 
        }
        if(!accToUpdate.isEmpty()){
          update accToUpdate;   
        }
       
    }
}