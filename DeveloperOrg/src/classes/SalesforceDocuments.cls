public with sharing class SalesforceDocuments {
    
     public SalesforceDocuments(ApexPages.StandardController stdController) {
        this.sd = (Salesforce_Documents__c)stdController.getRecord();
    }

    public Salesforce_Documents__c sd{get;set{
      sd =[select id from Salesforce_Documents__c limit 1];  
        }
                                     }
    public String name {get;set;}
    public string fname{get;set;}
    public transient blob attachDoc {get;set;}
    public  TRANSIENT string description {get;set;}
    public boolean errormessage{get;set;}    
    
    public   Attachment attachment {get;set;
  }
    

    /*public pagereference Items(){
        String str  ='%'+name+'%';
        items= [SELECT Id,Name,description,contentType FROM Attachment where name like :str];
        /*if(items.isEmpty()){
            system.debug('items are '+items);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'NO Record Found'));
            
        return null;
        }
      
        
        return null;
    }   
    */
    public pagereference UploadDoc(){
        String t='a082800000LPh6N';
       attachment = new Attachment();
        if(attachDoc!=null){
     attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = 'a082800000LPh6N'; 
   // attachment.IsPrivate = true;
     attachment.ContentType='application/msword';
        attachment.Name= fname;
        attachment.Body=attachDoc;
        attachment.Description=description;
       
        try{
            insert attachment;
           }catch(Exception e){
               system.debug('exc__'+e);
               errormessage=true;
        return null;
    // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please choose a file'));              
           } 
        }attachDoc=null;
        description=null;
        return ApexPages.currentPage().setRedirect(true);
    } 
  
    
}