global class CheckBoxInDetailController {

     global case cas  {get;set;}
     global Id caseID {get;set;}
    global CheckBoxInDetailController(ApexPages.StandardController casRec) {
         cas = (case)casRec.getRecord();
        
        
    }


   
    global CheckBoxInDetailController(){
       
    }
    global string BaseUrl {get;set;}
    global string grpName{get;set;}
    global boolean hideregion {get;set;}
    global boolean hideCheckBox {get;set;}
    global boolean checkBoxVal {get;set;}
    
    global pagereference showTextBox(){
    if(checkBoxVal==true){
    hideregion=true;
    hideCheckBox=false;
    }
    return Page.CheckBoxInDetailPage;
    }
    global pagereference createReltdLst(){
    
    caseID=Apexpages.currentpage().getparameters().get('id');
    system.debug('caseID'+caseID);
    String descrptn = 'case can been assigned to '+grpName+' queue';
    CaseComment comnts = new CaseComment();
    comnts.ParentId = caseID;
    comnts.CommentBody= descrptn;
     insert comnts;
    
    
     
     
    return null;
    }
    
    @RemoteAction
    global static  list<String> callGroups(string grpName){
        list<String> StrQueues;
        StrQueues = new list<String>();
        grpName='%'+grpName+'%';
        for(Group grp:[select Name from Group where Type = 'Queue' and name like :grpName]){
           StrQueues.add(grp.Name);
        }
        return StrQueues;
    }
}