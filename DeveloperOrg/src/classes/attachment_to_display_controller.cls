global class attachment_to_display_controller {

   //public  string  fileId{get;set;}
    public  Case acc {get;set;}
     
    public attachment_to_display_controller(ApexPages.StandardController controller) {
        acc = (case)controller.getrecord(); 
        system.debug('s'+acc);
    }
    
   public string getFileId() {
    String fileID='';
    ID i = ApexPages.currentPage().getparameters().get('id');
       system.debug('i'+i);
       List<Attachment> attachedFiles = [select Id,name from Attachment where parentId =:acc.id and ContentType='image/jpeg'order By LastModifiedDate DESC limit 1];
        system.debug(attachedfiles);
        if( attachedFiles != null && attachedFiles.size() > 0 ) {
            fileId = attachedFiles[0].Id;
        }
          return  fileId;
    }
    }