public class TriggerdeleteOpportunity_Handler {
   public  List<opportunity> triggernew;
    public List<opportunity> triggerold;
    map<id,opportunity> triggeroldmap;
    
 public TriggerdeleteOpportunity_Handler(List<opportunity>triggernew, List<opportunity> triggerOld,map<id,opportunity> triggeroldmap){
     this.triggernew=triggernew;
     this.triggerold = triggerold;
     this.triggeroldmap = triggeroldmap;
 }
   
    public void onbeforeInsert(){
        TriggerdeleteOpportunity_Handler.checkduplicate(triggernew,triggeroldmap);
        TriggerdeleteOpportunity_Handler.Fieldupdate(triggernew);
    }
    public void onbeforeUpdate(){
         TriggerdeleteOpportunity_Handler.Fieldupdate(triggernew);
    }
    
     public void onafterDelete(){
     TriggerdeleteOpportunity_Handler.ondeleteopportunity(triggernew,triggerold);
    }
    
    public static void Fieldupdate(List<opportunity> Triggernew){
        for(opportunity opp: triggernew){
            opp.CurrentGenerators__c='HelloWorld';
        }
        
    }
    public static void checkduplicate(List<opportunity> triggernew, map<id,opportunity> triggeroldmap){
     
        for(opportunity opp : triggernew){
         List<opportunity> oppname= [select name from opportunity where name=:opp.name limit 1];
                if(oppname.size()>0){
                opp.addError('duplicate value exist');  
            }
        }
    }
    
    public static void ondeleteopportunity(List<opportunity> triggernew,List<opportunity> triggerold){
      
        List<account >accList = new List<account>();
        set<id> setacc = new set<id>();
      map<id,id> mapid = new map<id,id>();
        for(opportunity opp : triggerold){
            if(opp.accountid!=null){
                setacc.add(opp.accountid);
            mapid.put(opp.accountid,opp.id);
            }
            
        }
        List<account> a = [select id,name,CustomerPriority__c from account where id =:setacc];
         for(account acc:a){
            if(mapid.containsKey(acc.id)){
                acc.CustomerPriority__c='high';
                accList.add(acc);
            }
            update accList;
        }
    }

}