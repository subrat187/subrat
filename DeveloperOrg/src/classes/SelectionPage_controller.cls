public class SelectionPage_controller {
    
   public ApexPages.StandardSetController con{get; set;}  
    public List<sobject> sobj {
        get{
            if(con != null)  
                return (List<sobject>)con.getRecords();  
            else  
                return null ;    
        } set;}
    public Map<String,Schema.SObjectField> mfields;
    public String searchobj{get;set;}
    public String fieldDescription {get;set;}
    public List<Selectoption> flist{get;set;}
    public List<String> selectedfield {get;set;}
    public String selectedobject{get;set;}
    public  list <String> objall {get;set;}
    public List<Selectoption> solist{get;set;}
    public List<Selectoption> fieldlist{get;set;}
    public map<String,Schema.SObjectType> mobj; 
    public set<String> sf;
    public SelectionPage_controller(){
        
        
        sobj = new List<sobject>();
        flist= new List<selectoption>();
        objall= new list<String>();
        solist= new List<selectoption>();
        mobj = schema.getGlobalDescribe();
        objall.addall(mobj.keySet());
        
        for(String s :objall){
            Selectoption so = new selectoption(s,s);
            solist.add(so);
            system.debug('obect is '+solist);   
        }
    }
    public void detailsobject(){
        schema.DescribeSObjectResult  dobj = mobj.get(selectedobject).getdescribe();
        mfields=dobj.fields.getMap();
        sf=mfields.keyset();
        for(string sfields:sf){
            Selectoption so = new selectoption(sfields,sfields);
            flist.add(so);
        }   
    }
    public void detailsfield() {
        for(String s:selectedfield ){
      schema.DescribeFieldResult df = mfields.get(s).getdescribe();
       List<Schema.PicklistEntry> ple = df.getPicklistValues();
          system.debug('fieldsdescrit************'+df);
            fieldDescription+=ple+',';
        }
  
      
    }
   
    public void search(){
        
        if(!selectedfield.isEmpty())  
        {  
            string query= 'select id';
            
            for(string s:selectedfield){
                query = query+' , '+s ;
            }
            
            query= query+' from '+selectedobject;
           
             system.debug('the sobject is'+query);
            con = new ApexPages.StandardSetController(database.query(query)); 
            
            
            con.setPageSize(10); 
            renderTable = true;
        }
        else 
              con=null;      
    }    
  public Boolean renderTable
    {
        get
        {
            if(renderTable == null)
                return false;
            return renderTable;
        }
        set;
    }

    public Boolean getHasNext()
    {
        if(con==null)
            return false;
        return con.getHasNext();
    }

   public Boolean getPrevious() {
    if(con==null)
                return false;
            return con.getHasPrevious();
  }
  public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
    
    
    public void first() {
        con.first();
    }
    
    
    public void last() {
        con.last();
    }
    
    
    public void previous() {
        con.previous();
    }
    
    
    public void next() {
        con.next();
    }
    public void cancel() {
        con.cancel();
    }
    
}