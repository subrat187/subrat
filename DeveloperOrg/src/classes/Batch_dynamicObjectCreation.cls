public class Batch_dynamicObjectCreation  implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
public string sessionid;
   String query;
    private Set<Id> Ids;
    public Batch_dynamicObjectCreation(Set<Id> ids,string sessionid){
        this.ids=ids;
        this.sessionid=sessionid;
     query= 'select id,SobjectName__c,slabel__c,DelField__c from DynamicCreationFieldMetadata__c where id IN :ids';
   
        system.debug('query'+query);
        
      
    }


public Database.querylocator start(Database.BatchableContext BC){

  return Database.getQueryLocator(query);
   }
   
    
public void execute(Database.BatchableContext BC, List<sObject> scope){
     List<custom_error_logs__c> lc = new list<custom_error_logs__c>();
    String message1;
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = sessionid;
    
     MetadataService.CustomObject customObject = new MetadataService.CustomObject();
      List< MetadataService.Metadata> customObjectList = new list<MetadataService.Metadata>();
       List< MetadataService.CustomObject> customObjectL = new list<MetadataService.CustomObject>();
     List<MetadataService.SaveResult> results;
    for( sobject sc:scope)
        {
           DynamicCreationFieldMetadata__c dc =(DynamicCreationFieldMetadata__c)sc;
            if(dc.DelField__c==true){
                system.debug('deleted');
                List<MetadataService.DeleteResult> delresults =
            service.deleteMetadata(
                'CustomObject', new String[] { dc.SobjectName__c });
        for (MetadataService.DeleteResult result : delresults) {
            if (!result.Success) {
                for(MetadataService.Error error : result.errors){
                    message1 =  'Error in creating field : Errors '+
                    'occured processing component '+error.message;   }     
       custom_error_logs__c errlog = new custom_error_logs__c(Name='metadataerror', error_description__c=message1);
                lc.add(errlog);        
                } 
            } 
                insert lc;
                lc.clear();
            }
            else{
        customObject.fullName = dc.SobjectName__c;
        customObject.label = dc.slabel__c;
        customObject.pluralLabel = dc.slabel__c+'s';
        customObject.nameField = new MetadataService.CustomField();
        customObject.nameField.type_x = 'Text';
        customObject.nameField.label = dc.slabel__c;
        customObject.deploymentStatus = 'Deployed';
        customObject.sharingModel = 'ReadWrite';
            customObjectList.add(customObject);
          
           system.debug('customobj'+customObjectList);
           results =service.createMetadata(customObjectList );
                    for (MetadataService.SaveResult result : results) {
            if (!result.Success) {
                 message1 =  'Error in Creating Object : Errors '+
                    'occured processing component ' ;   
                custom_error_logs__c errlog = new custom_error_logs__c(Name='metadataerror', error_description__c=message1);
                lc.add(errlog);        
                }
            
            } insert lc;
                lc.clear();
            }
        }
    
        //if condition
    
    
    }

     public void finish(Database.batchableContext bc){}


    
}