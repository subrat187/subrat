public class  VehicleMakePublic_Trigger{
    
    public static void makePublic(List<Vehicles__c> vech) {
   ID i = [SELECT Id FROM Group where Name='poudyalgroup'].id;

  // get the id for the group for everyone in the org
 
        
        if(Trigger.isInsert){
            
            list<vehicles__share> vs = new List<Vehicles__share>();
            for(vehicles__c v1:vech) {
                if(v1.Make_public__c==true){
            Vehicles__share v = new Vehicles__share();
            v.AccessLevel='edit';
                v.parentId= v1.id;
                v.UserOrGroupId=i;  
                    vs.add(v);
        }  
            }     
            if(!vs.isEmpty()){
            insert vs;
            }
        
        }
       
        if(Trigger.IsUpdate) {
            list<ID> l = new List<ID>();
            list<vehicles__share> vs = new List<Vehicles__share>();
            Vehicles__share v = new Vehicles__share();
            
            for(Vehicles__c vehicle : vech){
               // if it is checked and is public we need to convert it to private 
       Vehicles__C v1 =  (Vehicles__C) Trigger.oldmap.get(vehicle.id);
               
                if( v1.Make_public__c==true){
                    
                           l.add(vehicle.id);
                 }  
                 
           if(v1.Make_public__c==false){
           
            v.AccessLevel='edit';
                v.parentId= vehicle.id;
                v.UserOrGroupId=i;  
                    vs.add(v);
                    
                }
            }
                    
                if(!l.isEmpty()){
                   delete [Select parentid from vehicles__share where parentid in :l  AND RowCause = 'Manual'];
                   }              
         if(!vs.isEmpty()){
            insert vs;
            }    
        }
    }
}