public class PassByvalue {

    //string check
    public static void checkString(){
        string str ='adcda';
        list<string> dupStr = new list<string>();
        for(integer i=0;i<str.length()-1;i++){
            for(integer j=i+1;j<str.length();j++){
                 system.debug('dupStr'+str.substring(i,i+1)+'=='+str.substring(j,j+1));

                if(str.substring(i,i+1)==str.substring(j,j+1))  {
                    system.debug('strente');
                    dupStr.add(str.substring(i));
                }
                
            }
        }
        system.debug('dupStr'+dupStr);
    }
    public PassByvalue(){
       
       Account acc = new Account(name = 'Acme', description = 'Acme Account');
Schema.sObjectType expected = Schema.Account.getSObjectType();
System.debug('expected'+acc.getSobjectType()); 
    }
       public PassByvalue(integer a){
       system.debug('a'+a); 
    }
    
    
    @future
    public static void mains(){
        PassValues();
        
    }
    
     
    public static void main1s(){
        
        mains();
    }
      integer a=10;
   
    public  void m(){
        system.debug('a'+a);
        setM(8);
        getM();
        system.debug('a'+a);
    }
    
    public integer  getM(){
        system.debug('a'+a);  
        return a;
    }
    public void setM(integer m){
       a=m; 
    }
    
  public static void PassValues(){
   //  PassByReference pagref = new PassByReference(10,'cog');
    integer [] pagref= new integer[]{10,20};
      system.debug('passref--'+pagref);
      passByRef(pagref);
      system.debug('passref---1'+pagref);
     
          
       integer val =10;
       system.debug('val---1'+val);
        passByVal(val);
       system.debug('val---2'+val);
      
    }
    public static void passByRef(integer [] val){
       
       // val=new PassByReference(2,'wipro');
        val= new integer[]{30,40};
          system.debug('passByRef--3'+val);
    } 

    public static void passByVal(integer val){
        
          val = 20;
      system.debug('val---after'+val);
    
          
    }
}