public class MergeApex {
    
    public static void main(){


// Queries to get the inserted accounts 
Account masterAcct = [SELECT Id,(select id from contacts) FROM Account WHERE Name = 'Acme Inc.' LIMIT 1];
      map<ID,contact> cont = new map<ID,contact>([select accountid from contact where account.name='Acme Inc']);
        list<Id> acId = new list<ID>();
        list<Id> accId = new list<ID>();
        for(Contact con :cont.values()){
            if(con.accountID!=null){
              acId.add(con.accountID); 
            }   
        }
        if(masterAcct.contacts==null){
            accId.add(masterAcct.id);
        }
Account mergeAcct = [SELECT Id, Name FROM Account where id in :acId];

try {
    system.debug('masterAcct'+masterAcct);
     system.debug('mergeAcct'+mergeAcct);
    
    merge masterAcct mergeAcct;
} catch (DmlException e) {
    // Process exception
    System.debug('An unexpected error has occurred: ' + e.getMessage()); 
}

// Once the account is merged with the master account,
// the related contact should be moved to the master record.
masterAcct = [SELECT Id, Name, (SELECT FirstName,LastName From Contacts) 
              FROM Account WHERE Name = 'Acme Inc.' LIMIT 1];
System.assert(masterAcct.getSObjects('Contacts').size() > 0);
System.assertEquals('Joe', masterAcct.getSObjects('Contacts')[0].get('FirstName'));
System.assertEquals('Merged', masterAcct.getSObjects('Contacts')[0].get('LastName'));

// Verify that the merge record got deleted
Account[] result = [SELECT Id, Name FROM Account WHERE Id=:mergeAcct.Id];
}
        
    }