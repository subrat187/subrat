global class Update_all implements Database.Batchable<sobject>{
 
 
 global String query;
 global String s_objects;
global String fieldname;
 global String fieldvalue;
 
  global Update_all(String query,String s_objects,String fieldname,String fieldvalue ){
  
  this.query=query;
  this.s_objects=s_objects;
  this.fieldname=fieldname;
  this.fieldvalue= fieldvalue;
  }
 
  global database.queryLocator start(database.BatchableContext bc){
  
  return database.getQueryLocator(query);
  }
  
  global  void execute(database.BatchableContext bc,List<sobject> batch){
  
  for(sobject s :batch){
   s.put(fieldname,fieldvalue);
 
  }
    update batch;
 }
  global void finish(database.BatchableContext bc){
  }
 
 
 
 }