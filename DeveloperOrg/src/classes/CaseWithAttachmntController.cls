public class CaseWithAttachmntController {
    
    private Case cas {get;set;}
    public attachment caseAttachment1 {get;set;}
    public attachment caseAttachment2 {get;set;}
    public attachment caseAttachment3 {get;set;}
    
    public CaseWithAttachmntController(Apexpages.StandardController sc ){
        cas=(case)sc.getRecord();
        if(caseAttachment1==null){
            caseAttachment1 = new attachment();
        }
         if(caseAttachment2==null){
            caseAttachment2 = new attachment();
        }
         if(caseAttachment3==null){
            caseAttachment3 = new attachment();
        }
    }
    
    public pagereference save(){
        try{
             Database.SaveResult dataInsrted= database.insert(cas); 
        Id casid=dataInsrted.getId();
        system.debug('caseAttachment1'+caseAttachment1);
        if(caseAttachment1.Name!=null && caseAttachment1.Body!=null){
             caseAttachment1.OwnerId = UserInfo.getUserId();
            caseAttachment1.ParentId = casid;
            insert caseAttachment1;
        }
          if(caseAttachment2.Name!=null && caseAttachment2.Body!=null){
              caseAttachment2.OwnerId = UserInfo.getUserId();
            caseAttachment2.ParentId = casid;
            insert caseAttachment2;
        }
          if(caseAttachment3.Name!=null && caseAttachment3.Body!=null){
             caseAttachment3.OwnerId = UserInfo.getUserId();
            caseAttachment3.ParentId = casid;  
            insert caseAttachment3;
        }   
        String instnceUrl = String.valueOf(System.URL.getSalesforceBaseURL().toExternalForm());
        system.debug(' System.URL.getSalesforceBaseURL()'+instnceUrl);
        pagereference pagref = new pagereference(instnceUrl+'/'+casid);
        return pagref;
        }
        catch(Exception ex){
            system.debug('exception in save'+ex);
            return null;
        }
        
    }
    public pagereference cancel(){
        try{
        String instnceUrl = String.valueOf(System.URL.getSalesforceBaseURL().toExternalForm());
        pagereference pagref = new pagereference(instnceUrl+'/500/o'); 
        return pagref;
    }catch(Exception ex){
            system.debug('exception in Cancel'+ex);
        return null;
        }
    }
}