public class GetterSetter {

    public String name {
        get{
            return name;
        }
        set{
            name = value; 
        }
    }
  
    @AuraEnabled
    public static list<Account> getAccounts(){
        return [select name from account limit 10];
    }
    

     @AuraEnabled
    public static ID saveTheFile(String parentId,String fileName, String base64Data, String contentType) { 
      system.debug('fileinserted');
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = new Attachment();
        a.parentId = parentId;

        a.Body = EncodingUtil.base64Decode(base64Data);
        a.Name = fileName;
        a.ContentType = contentType;
        
        insert a;
        SYSTEM.debug('AAA'+a.Id);
       return a.Id; 
    }
   
}