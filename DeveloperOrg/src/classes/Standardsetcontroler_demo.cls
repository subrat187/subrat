public class Standardsetcontroler_demo {

    public Apexpages.StandardSetController rec {get;set;}
    public string  selected{get;set;}
    public string []selectedfield{get;set;}
    public Map<string,schema.SObjectField> mfield {get;set;}
    public map<string,schema.SObjectType> sobj{get;set;}
    public Set <string> sobjkey;
    public list<selectoption>sobjlist {get;set;}
     public list<selectoption>fieldlist {get;set;}
     public Set <string>fieldkey;
    public Standardsetcontroler_demo (){
       mfield =new Map<string,schema.SObjectField>();
        fieldlist= new list<selectoption>(); 
       sobjlist = new list<selectoption>();
        sobjkey = new Set<string>();
        fieldkey =new set<string>();
        sobj=schema.getGlobalDescribe();
        sobjkey.addall(sobj.keySet());
         selectoption sel = new selectoption('none','none');
           sobjlist.add(sel);
        for(string s:sobjkey){
           selectoption  sel1 = new selectoption(s,s);
        
            sobjlist.add(sel1);
        }
    }
    
    
    
    
    public List<sobject> rec1 { get{
        if(rec!=null){
            return rec.getrecords();
        }
        else{
            return null;
        }
    }
           set;}
    
    public void fetchrecords(){
       mfield.clear();
        fieldlist.clear();
         fieldkey.clear();
       system.debug('entered');
        if(selected!='none'){  
            system.debug('selected');
    mfield = sobj.get(selected).getdescribe().fields.getMap();
            system.debug('ss'+mfield.keyset());
        }
 
     fieldkey.addall(mfield.keySet()); 
        system.debug(fieldkey);
        
        for(string sfield:fieldkey){
            selectoption sf= new selectoption(sfield,sfield);
            fieldlist.add(sf);
        }
        
        
    }
    
    public void searched(){
         String query;
        if(selectedfield!=null){
         query ='select id ';
            for(string sel:selectedfield){
                if(sel!='id'){
              query+=' , '+sel;
            }}
                
           query+=' from '+selected; 
            system.debug('query	='+query);
       rec=new Apexpages.StandardSetController(database.query(query)) ;   
            system.debug('rec'+rec);
        }
       
        rec.setPageSize(10);    
    }
    public Boolean gethasnext(){
        if(rec==null){
            return false;}
        else { return rec.getHasNext();}
    }
    public void next(){
        rec.Next();
    }
         public void previous() {
        rec.previous();
    }
    
    public integer page(){
      return  rec.getPageNumber();
    }   
    public void cancel() {
        rec.cancel();
    }
    }