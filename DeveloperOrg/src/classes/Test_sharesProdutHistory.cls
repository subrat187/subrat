@isTest

private class Test_sharesProdutHistory {
 
 static testmethod void constructorTest() {
 
 Account a = new Account (Name='SubratInc' ,Phone='552444',NumberofLocations__c=3);
 insert a;
 
 List<Campaign> c = new List<Campaign>();
 Campaign obj = new Campaign( BudgetedCost=10000,Description='first controller test method',Name='SubratCamp' );
 c.add(obj);
 insert c;
 Test.StartTest();
 ApexPages.StandardController sc = new ApexPages.StandardController(a);
  Accounts_Campaign ac = new Accounts_Campaign(sc );
  pageReference pageref = Page.sharesProdutHistory;
 
  pageRef.getParameters().put('id', String.valueOf(a.Id));
  Test.setCurrentPage(pageRef);
  
  ac.getcamp();
  Test.StopTest();
  
       }
 }