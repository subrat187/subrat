@RestResource(urlMapping='/FieldCase/*')
global with sharing class RESTCaseController {
    public void m(){ system.debug('started');}
    
    @HttpPost  
    global static String createNewCase(String companyName) {
         system.debug('entered');
       RestResponse res = RestContext.response;
        system.debug('res');
        System.debug('COMPANY: '+companyName);
     
        Account company = [Select ID, Name, BillingState from Account where Name = :companyName limit 1];
        User u = [select name,id from user where accountid= :company.id];
        if(company.id == null ) {
             return 'No support data exists for this problem';
        }
        //https://subratpoudyal-dev-ed.my.salesforce.com/_ui/common/apex/debug/ApexCSIPage#
      
        Case c = new Case();
        c.OwnerId = u.id;
        c.AccountId = company.Id;
        c.Subject = ' for '+companyName;
        c.Status = 'Open';
          system.debug('REsponseBog'+RestContext.response.responseBody);
        insert c;
        return 'Submitted case to '+u.name+' for '+companyName ;
        
    }
    
}