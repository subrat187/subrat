@isTest
public class TestFactory {
    public static  testmethod opportunity getOpportunity(){
        Opportunity opp = new Opportunity();
        opp.name='subratopp1';
        opp.StageName='Prospecting';
        opp.Type='Existing Customer - Upgrade';
        opp.Amount=500.00;
        opp.CloseDate=system.today();
        return opp;
    }
}