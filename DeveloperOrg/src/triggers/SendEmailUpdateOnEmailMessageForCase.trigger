trigger SendEmailUpdateOnEmailMessageForCase on EmailMessage (before insert, after insert) { 

	try{	
		for(EmailMessage e: trigger.new){
       
            
			string caseId = string.valueof(e.ParentId);
						
			if(caseId.startsWith('500')){	//This Email Message is for a Case 
				
				//Look up Case Details
				Case theCase = [SELECT Id, ContactId, OwnerID, Subject, Priority, IsClosed, EntitlementId, Description, CreatedDate, CaseNumber, AccountId  FROM Case WHERE Id =: caseId Limit 1];			
				
			
				
			 if(trigger.isAfter){
					
					
					Set<ID> toEmailId = new Set<ID>();
					toEmailId.add(theCase.OwnerId);
					//Get Ids of the Case Team Members
			
					//system.debug('%%%List of Members %%%%% ' + toEmailId);
					
					//Get contact and user list from the set of Id's			
				
					List<User> listUserEmails =  [SELECT Email FROM User Where ID IN: toEmailId]; 
		
					//Get all email address in unique set
					
					Set<String> setOfUserEmails = new Set<String>();
				
					for(User u: listUserEmails){
						setOfUserEmails.add(u.Email);
					}
								
					list<String> listOfEmails = new list<String>();
				
					listOfEmails.addAll(setOfUserEmails);
					listOfEmails.sort();		
				
					//Create the email message body
					Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
					EmailTemplate emailTemp = [Select id From EmailTemplate  Limit 1]; //A email template for Cases
					email.orgWideEmailAddressId = '0D2D0000000GnPW';	//Our Org Wide default
					email.replyTo = 'support.test@ictnetworks.com.au';
		      		        email.saveAsActivity = false;
					email.settargetObjectId(theCase.ContactId);
					email.setCcAddresses(listOfEmails);
					email.setTemplateId(emailTemp.Id);
					email.setwhatId(theCase.Id);
					
					//Check for Attachments							
					system.debug('H$H$H$H$H Attachment Flag: ' + e.HasAttachment);
					if(e.HasAttachment){
						//Create list of Messageing email File Attachments
						list<Messaging.EmailFileAttachment> fileAttachments = new list<Messaging.EmailFileAttachment>();
						//Query all child Attachments relating to this email Id
					        list<Attachment> allAttachmentsInEmail = [Select Name, ContentType, Body From Attachment Where parentId =:e.id]; 
						//Add to list of Attachments to email
						system.debug('---->number of attachments: ' + allAttachmentsInEmail.size());
						for(Attachment a: allAttachmentsInEmail){
							Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
							efa.setFileName(a.Name);
							efa.setBody(a.Body);
							fileAttachments.add(efa);
						}
						email.setFileAttachments(fileAttachments);
					}	
					
					system.debug('@@@@@targetI ObjectID = ' + email.targetObjectId);
					system.debug('@@@@@emailTemp.Id = ' + emailTemp.Id);
					system.debug('@@@@@whatId = ' + email.whatId);
					//email.setPlainTextBody('Hello!'); 
					//email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa}); 
					//system.debug(email); 
					//Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
										
				}
			}
		}
	} catch (exception e){
		System.debug('The following exception has occurred: ' + e.getMessage());
	}
}