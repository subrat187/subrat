trigger TriggerdeleteOpportunity on Opportunity (before insert,before update,after insert,after delete) {

 TriggerdeleteOpportunity_Handler tdopp = new TriggerdeleteOpportunity_Handler(Trigger.new,Trigger.old,Trigger.oldMap);
    
    if(Trigger.isdelete  && trigger.isAfter){
        tdopp.onafterDelete();
    }
    
    if(trigger.isinsert && trigger.isBefore){
        tdopp.onbeforeInsert();    
    }
    
    if(trigger.isUpdate && trigger.isBefore){
        tdopp.onbeforeUpdate();
    }
}