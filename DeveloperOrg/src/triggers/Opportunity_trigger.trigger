trigger Opportunity_trigger on Opportunity (before insert,before update) {

    opportunity_triggerhandler opp = new opportunity_triggerhandler(Trigger.new,Trigger.oldMap);
    if(Trigger.isBefore){
         if(Trigger.isInsert){
        opp.onBeforeInsert();
         }  
    }
    if(Trigger.isBefore){
    if(Trigger.isUpdate){
        opp.onBeforeUpdate();
    }
    }
   
}